= Darstellen von grossen Punktmengen als Punkt-Cluster und als Heatmap
OpenSchoolMaps.ch -- Freie Lernmaterialien zu freien Geodaten und Karten
:xrefstyle: short
:imagesdir: ../../../bilder/rendering_points_with_qgis/
:experimental:
include::../../../snippets/suppress_title_page.adoc[]
*Ein Arbeitsblatt*


== Überblick

In diesem Arbeitsblatt wird erklärt, wie man bestimmte Punkt-Rendering-Techniken 
einsetzen kann, um grosse Mengen von Punkten darzustellen.
Wir werden verschiedene sogenannte "Punkt-Renderer"-Funktionen von QGIS nutzen,
um zu zeigen, wie solche Herausforderungen bewältigt werden können.

Bei dieser Übung werden Daten von Schweizer Burgen verwendet (archäologische Stellen, 
Befestigungen, Türme, etc.).
Die Übung wird einige der wichtigsten Punkt-Rendering-Techniken von QGIS erklären 
und demonstrieren:

* Punkt-Cluster-Renderer
* Punktverdrängungs-Renderer 
* Heatmaps


== Lernziele

Nach dem Ausfüllen dieses Arbeitsblattes kannst du:

* verstehen, welche Herausforderungen Datenamsammlungen mit sich bringen und wie man diese bewältigen kann,
* wichtigste Punkt-Darstellungs-Techniken effizient mit QGIS verwenden,
* verstehen, welche Punkt-Rendering-Techniken welche Probleme lösen.


== Einleitung

Die meisten Objekte in der realen Welt können als Punkte dargestellt werden,
trotzdem können diese viele Herausforderungen mit sich bringen.
Diese unterschiedlichen Herausforderungen können mit bestimmten Techniken und
Renderers überwunden werden.
In diesem Arbeitsblatt werden wir einige von den wichtigsten QGIS-Renderer beschreiben und erklären.
Wir haben einige historische Plätze in der Schweiz als spezielle Beispiele genommen, d.h. im Detail:
Burgen, Türme, archäologische Plätze, Festungen etc.
Das sind schöne Beispiele, um zu demonstrieren, was Punkte sind
und welche Rendering-Techniken gebraucht werden können, um die Probleme zu lösen,
die sie bereiten können.

QGIS bietet einige nützliche Punkt-Rendering-Techniken an.
Solche Techniken werden hauptsächlich zum Umgang mit beschränktem Platz auf dem Bildschirm
und zur Unterscheidung von verschiedenen Punkten auf engem Raum eingesetzt.
Sie können auch verwendet werden, nachgefragte Bereiche von weniger nachgefragten Bereichen zu unterscheiden.

.Eine Sammlung(Layer) von Punkten auf einer Karte in QGIS.
image::qgis_points.png[QGIS and a points layer]


== Vorbereitung

Um dieses Arbeitsblatt zu verstehen und lösen, brauchst du die folgenden Vorbereitungen:

* Software: QGIS Desktop (getestet mit QGIS 3.10.1-A Coruña)
* Daten: `castles.geojson` Datei

Nachdem du das geschafft hast, werden wir die nötigen Daten ins QGIS laden
und mit den Erklärung beginnen.

=== Daten bereitstellen

Die für dieses Arbeitsblatt verwendeten Geodaten sind im `.geojson`-Format.
Sie sind aus Overpass-Turbo (_overpass-turbo.osm.ch_), einer einfachen Webapp zum Herunterladen 
von OSM-Daten in verschiedenen Formaten.
Die Daten werden also direkt aus OpenStreetMap übernommen, inklusive der Attribute (tags).
QGIS kann `.geojson` Dateien lesen und in der Karte verarbeiten. 

NOTE: Um die Overpass-Abfrage der OSM-Daten in der `.geojson` Datei anzuschauen,
kannst du folgenden Link verwenden: https://osm.li/3FH

Eine `.geojson` Datei sieht so aus:

[source, json]
----
include::daten/castles.geojson[lines=5..35]
----
NOTE: Weitere Informationen zur Dateiendung `.geojson` findest du hier: https://geojson.org/

=== Einrichten

Zum Einrichten der Umgebung und QGIS musst du folgendes tun:

1. Öffne QGIS und stelle sicher, dass kein schon bestehendes Projekt geöffnet ist. 
2. Lade die Datei `castles.geojson` (oder exportiere sie manuell mit Overpass Turbo: https://osm.li/3FH) mit Drag&Drop in QGIS.
3. Wähle einen Kartenhintergrund, damit wir sehen können, wo sich die Punkte auf der Weltkarte befinden. 

Jetzt bist du bereit loszulegen!


== Punkt-Cluster-Rendering

Eine der wichtigsten Punkt-Darstellungs-Techniken ist das _Punkt-Cluster-Rendering_.
Diese Technik ersetzt einander nahestehenden Punkte durch ein einziges Symbol.
Dadurch wird das Kartenbild entlastet, da das Symbol Platz spart.

Wähle für den Layer das _Punkt-Cluster-Rendering_ mit dem Stil-Menu.  
Nun kannst du mit dem Parameter _Distance_ in den Optionen herumspielen und sehen, 
welchen Unterschied es macht.

Der Parameter, der festlegt wie nahe die Punkte sein sollen, um zu einem Cluster 
konvertiert zu werden, kann angepasst werden.

Hier siehst du als Beispiel Burgen und Schlösser der Schweiz nach Anwendung der Punkt-Cluster-Rendering-Technik. 

.Burgen und Schlösser der Schweiz dargestellt mit der Punkt-Clustering-Technik.
image::point_cluster_map.png[Point Cluster technique]

QGIS bietet uns auch einige Optionen für diese Rendering-Technik.
Dies erlaubt es uns, folgende Änderungen vorzunehmen:

* Cluster-Symbol
* Distanz zum Auslösen des Clusters
* Renderer-Typ etc.

.Punkt-Cluster-Optionen von QGIS.
image::point_cluster_options.png[Point cluster options, 450] 


== Punktverdrängungs-Rendering

Eine andere wichtige Punkt-Rendering-Technik heisst _Punktverdrängungs-Renderer_.

Angenommen, wir haben viele Punkte innerhalb eines Kartenlayers.
Wenn wir herauszoomen, kommen viele Punkte übereinander zu liegen, sie überlappen.
Damit lassen sie sich beispielsweise nicht mehr gezielt selektieren.
Das ist der Moment, wo wir die Punktverdrängungs-Technik anwenden.

Mit dieser Rendering-Technik werden überlappende Punkte in der Kartenansicht 
gruppiert und mit einem 'Mittelsymbol' versehen. 
Beim Draufklicken werden Symbole werden kreisförmik um den Mittelpunkt herum platziert.

.Burgen und Schlösser der Schweiz dargestellt mit der Verdrängungs-Render-Technik
image::point_displacement_map.PNG[Point displacement technique]

.Ein Beispiel dafür, wie überlappende Punkte nach Verwendung von Punktverdrängungs-Renderer mit einem Mittelsymbol gerendert werden.
image::point_displacement_example.png[Point displacement example, 180] 

Der Punktverdrängungs-Renderer bietet uns auch einige Optionen.
Mit diesen können wir folgendes machen:

* Symbolform/-farbe zentrieren
* Renderer-Modus für die Punkte definieren
* Distanz zum Verschieben der überlappenden Punkte festlegen
* Plazierungsmethode wählen
* Linienform und -farbe der Verschiebung auswählen
* Beschriftung, etc. ändern

.Punktverdrängungs-Optionen von QGIS.
image::point_displacement_options.png[Point displacement options, 300] 


== Heatmap

Eine weitere wichtige Rendering-Technik ist die Darstellung von Punkten als Heatmap.
Diese Technik ist eine beliebte Visualisierung für dichte Punktdaten.

Eine Heatmap-Rendering-Technik ist nützlich für die rasche Identifikation von 
Punkte-Gruppen mit hoher Dichte (Nähe).
Das kann für einige Szenarien nützlich sein, wie zum Beispiel das Lokalisieren von 
gefährlichen Gegenden (Delikte/Verbrechen), Verkehrsunfällen, Wohnungsdichte, usw.

Wir werden die gleichen Geodaten wie bisher nutzen, um die Heatmap zu demonstrieren 
(Burgen und Schlösser der Schweiz).

Wähle nun diese Option für den Layer und spiele mit dem _Radius_ Parameter in 
Rendering-Optionen herum um die Unterschiede zu sehen.

.Heatmap mit Burgen und Schlösser der Schweiz.
image::heatmap_generation_map.png[Heatmap generation technique]

Nach der Generierung der Heatmap werden einige Optionen in QGIS angezeigt.
Diese Optionen erlauben es uns, folgendes zu setzen: 

* Farbe der Heatmap und des Hintergrunds
* Qualität der Heatmap-Darstellung
* Maximaler Wert
* Wie man Punkte abwägt usw.

.Heatmap-Erstellungsoptionen von QGIS.
image::heatmap_generation_options.png[Heatmap generation options, 350]


<<<


== Übungen

Im Übungsabschnitt, werden wir andere Geodaten verwenden, damit du ein besseres 
Verständnis dafür bekommst, wie Punkte funktionieren und wie die Rendering-Techniken 
helfen können, einige Punktprobleme zu lösen: 
Wir werden Tischtennisdaten der Schweiz verwenden.

=== Vorbereitung: Anforderungen vor Beginn der Übungen

1. Erstelle ein neues leeres QGIS-Projekt.
2. Lade die Datei `table_tennis.geojson` (oder exportiere sie manuell mit Overpass 
Turbo: https://osm.li/5XH) mit Hilfe der Drag&Drop-Technik in QGIS.
3. Wähle einen Kartenhintergrund, damit wir sehen können, wo sich die Punkte auf 
der Weltkarte befinden.

=== Übung

Spiele mit allen Punkt-Rendering-Techniken, die im Hauptteil des Arbeitsblatts 
erwähnt wurden, herum und beantworte die folgenden Fragen: 

1. Finde Ansammlungen (Cluster) von Tischtennisplatten in der Schweiz.
2. Erstelle eine Heatmap, um die Hotspots von Tischtennisplatten der Schweiz zu finden.
3. Finde heraus, welche Stadt der Schweiz die meisten Tischtennisplatten hat.


== Abschluss 

Mit diesem Arbeitsblatt hast du gelernt, wie Punkte in der Karte visualisiert werden
und wie wir verschiedene Punkte-Rendering-Techniken verwenden können, um bestimmte 
Probleme, die Punkte stellen können, zu lösen.

Jede der Punkt-Rendering-Techniken löst ein bestimmtes Probleme.
Es gibt keine Punkt-Rendering-Technik, die alle Probleme lösen kann.
Das bedeutet, dass man abwägen muss, welche Technik die Geeignetste ist.

<<<


== Ausblick

=== Einige andere Datensätze

Hier sind noch einige andere Datensätze, die verwendet werden können, um zu zeigen, 
wie Punkte und auch die verschiedenen QGIS-Punkte-Rendering-Techniken funktionieren:

* Fussballplätze in der Schweiz: https://osm.li/BYH
* Basketballplätze in der Schweiz: https://osm.li/CYH
* Nachtclubs in der Schweiz: https://osm.li/DYH
* Touristenattraktionen in der Schweiz: https://osm.li/GYH

NOTE: Alle oben genannten Datensätze können ebenfalls in `.geojson` Dateien exportiert werden,
durch Klicken auf die btn:[Export]-Schaltfläche in _Overpass Turbo_.
Sie können dann per Drag&Drop in QGIS geladen werden.


---

Noch Fragen? Wende dich an OpenStreetMap Schweiz (info@osm.ch)
oder an Stefan Keller (stefan.keller@ost.ch).

:imagesdir: ../../../bilder/

include::../../../snippets/license.adoc[]
