= QGIS Cheatsheet
:author: {blank}
:experimental:
:imagesdir: ../../bilder/qgis_cheatsheet/
include::../../snippets/suppress_title_page.adoc[]

== GUI Beschreibung
image::qgis_description_screenshot.png[QGIS Description]

There are more Layers to the GUI of QGIS which arent visible. To open these click on "View > Panels". 
To open more Toolbars you are able to open them by clicking on "View > Toolbars"

A different method is to right click on a grey bar in the toolbar area. Now you should be able to see both Panels and Toolbars.

<<<

=== Status bar
image::status_bar.png[QGIS Status bar]

From left to right:

. Coordinate: Show the coordinates your mouse is hovering over in the map window. The coordinates are in reference to the choosen Coordinate Reference System (CRS).
. Scale: Shows the scale on which the map is visible on the map view. The scale can be changed by either zooming in, zooming out or by selecting one of the predefined options in the dropdown
. Magnifier: By clicking on the image:qgis_lock.png[lock] lock you can enable the option to zoom in and out without altering the scale. The Default value is 100%.
. Rotation: Can be used to rotate the map clockwise.
. Rendering: Can be used to disable the rendering of the map temporarily.
. By clicking on the image:qgis_crs.png[crs_icon] the dialog for the selection of the Coordinate Reference System opens. 
. By clicking the image:qgis_bubble.png[message bubble] the log messages show up.

image::qgis_log_messages.png[Log Messages]

== Menu and Toolbar

=== Navigation in the map view

.Navigation in the map view
|===
| Name | Menu option | Shortcut | Description

|Map pan
a| image::qgis_pan_map.png[Pan Icon, 30, 30]
| kbd:[Space], kbd:[Page Up], kbd:[Page Down] or the Arrow Keys
| Move the map

|Pan map to selection
a| image::qgis_pan_map_selection.png[Pan Map to Selection]
| 
| Pans the map to the selected element

|Zoom in
a| image::qgis_zoom_in.png[Zoom in, 30, 30]
| kbd:[Ctrl + Alt + +] or mouse wheel
| Zoom into the map

|Zoom out
a| image::qgis_zoom_out.png[Zoom out, 30, 30]
| kbd:[Ctrl + Alt + -] or mouse wheel
| Zoom out of the map

|Zoom full
a| image::qgis_zoom_full.png[Zoom full, 30, 30]
| kbd:[Ctrl + Shift + F]
| Zoom out to show the full map

|Zoom to selection
a| image::qgis_zoom_to_selection.png[Zoom to selection, 30, 30]
| kbd:[Ctrl + J]
| Zoom to the selected element

|Zoom to layer
a| image::qgis_zoom_to_layer.png[Zoom to layer, 30, 30]
|
|Zoom to the selected layer

|Zoom to native resolution
a| image::qgis_zoom_native_resolution.png[Zoom to native resolution, 30, 30]
|
|Zoom to the native resolution (100%)

|Zoom last
a| image::qgis_zoom_last.png[Zoom last, 30, 30]
|
| Zoom to the last zoom

|Zoom next
a| image::qgis_zoom_next.png[Zoom next, 30, 30]
|
|Zoom to the next

|===

=== Project managment

.Project managment
|===
| Name | Menu option | Shortcut | Description

| New Project
a| image::qgis_new.png[New project, 30, 30]
| kbd:[Ctrl + N]
| Create a new project

| Open Project
a| image::qgis_open_project.png[Open project, 30, 30]
| kbd:[Ctrl + O]
| Open an existing project Opens the Dialog to create a new print layout

| Save
a| image::qgis_save_project.png[Save, 30, 30]
| kbd:[Ctrl + S]
| Save the project

| Save as...
a| image::qgis_save_project_as.png[Save as..., 30, 30]
| kbd:[Ctrl + Shift + S]
| Save the project as...

| Properties
a|
| kbd:[Ctrl + Shift + P]
| Open the project properties

| New print layout
a| image::qgis_new_print_layerout.png[New print layout, 30, 30]
| kbd:[Ctrl + P]
| Opens the Dialog to create a new print layout

| Search
a|
| kbd:[Ctrl + K]
| Opens the search bar

|===

=== Layer management

.Layer management
|===
| Name | Menu option | Shortcut | Description

|Data source manager
a| image::qgis_data_source_manager.png[Data source manager, 30, 30]
| kbd:[Ctrl + L]
| Add a new layer

| New GeoPackage layer
a| image::qgis_new_geopackage_layer.png[New GeoPackage layer, 30, 30]
| kbd:[Ctrl + Shift + N]
| Add a new GeoPackage Layer

| Add vector layer
a| image::qgis_add_vector_layer.png[Add vector layer, 30, 30]
| kbd:[Ctrl + Shift + V]
| Add a new vector layer

| Add raster layer
a| image::qgis_add_raster_layer.png[Add vector layer, 30, 30]
| kbd:[Ctrl + Shift + R]
| Add a new raster layer

| Remove selected layer
a| image::qgis_remove_selected_layer.png[Remove selected layer, 30, 30]
| kbd:[Ctrl + D]
| Remove the selected layer

| Toggle layers view
a| 
| kbd:[Ctrl + 1]
| Toggle the layers view

| Toggle browser view
a|
| kbd:[Ctrl + 2]
| Toggle the browser view

|===

.Analysis Tools
|===
| Name | Menu option | Shortcut | Description

| Identify Features
a| image::qgis_identify_features.png[Identify features, 30, 30]
| kbd:[Ctrl + Shift + I]
| Identify features on the map view by clicking on them

| Select feature
a| image::qgis_select_features.png[Select features, 30, 30]
|
| Select a features by area or single click

| Select feature by value
a| image::qgis_select_features_by_value.png[Select features by value, 30, 30]
| kbd:[F3]
| Select features by value

| Open Attribute table
a| image::qgis_open_attribute_table.png[Open Attribute table, 30, 30]
| kbd:[F6]
| Open the Attribute table

| Open Attribute table with selected features only
a| image::qgis_open_attribute_table.png[Open Attribute table, 30, 30]
| kbd:[Shift + F6]
| Open the Attribute table with selected features only

| Open Attribute table with visible features only
a| image::qgis_open_attribute_table.png[Open Attribute table, 30, 30]
| kbd:[Ctrl + F6]
| Open the Attribute table with visible feautres only

|===

=== Advanced Tools

.Advanced Tools
|===
| Name | Menu option | Shortcut | Description

| Processing Toolbox
a| image::qgis_processing_toolbox.png[Processing Toolbox, 30, 30]
| kbd:[Ctrl + Alt + T]
| Opens the Processing Toolbox

| Python Console
a| image::qgis_python_console.png[Python Console, 30, 30]
| kbd:[Ctrl + Alt + P]
| Opens the Python Console

|===

For more shortcuts and information read the https://www.qgis.org/en/docs/index.html[QGIS documentation]

:imagesdir: ../../bilder/

include::../../snippets/license.adoc[]
